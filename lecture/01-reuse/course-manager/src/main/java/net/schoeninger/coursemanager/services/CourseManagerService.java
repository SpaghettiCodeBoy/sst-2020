package net.schoeninger.coursemanager.services;

import net.schoeninger.coursemanager.domain.Course;
import net.schoeninger.coursemanager.domain.Review;

import javax.validation.Valid;
import java.util.List;

public interface CourseManagerService {

    Course saveCourse(@Valid Course course);
    List<Course> findAllCourses();
    Review addReviewToCourse(@Valid Review newReview, long courseId);
    List<Review> findAllReviewsForCourse(long courseId);

}

package net.schoeninger.coursemanager;

import lombok.extern.slf4j.Slf4j;
import net.schoeninger.coursemanager.domain.Course;
import net.schoeninger.coursemanager.domain.Review;
import net.schoeninger.coursemanager.repositories.CourseRepository;
import net.schoeninger.coursemanager.repositories.ReviewRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Slf4j
@Component
public class SampleDataGenerator implements CommandLineRunner {

    private CourseRepository courseRepository;
    private ReviewRepository reviewRepository;

    public SampleDataGenerator(CourseRepository courseRepository, ReviewRepository reviewRepository) {
        this.courseRepository = courseRepository;
        this.reviewRepository = reviewRepository;
    }

    @Override
    public void run(String... args) {
        log.info("Adding sample data to the DB..");
        List<Course> courses = addCourseSamples();
        addReviewSamples(courses, 10);
        log.info("Finished adding sample data to the DB!");
    }

    private List<Course> addCourseSamples() {
        List<Course> courses = List.of(
                Course.builder().title("SST").build(),
                Course.builder().title("Software Architecture").build(),
                Course.builder().title("Data Mining").build(),
                Course.builder().title("IT Management").build(),
                Course.builder().title("Innovation Management").build()
        );
        return courseRepository.saveAll(courses);
    }

    private void addReviewSamples(List<Course> courses, int numberOfReviews) {
        List<Review> reviews = new ArrayList<>();
        Random random = new Random();
        courses.forEach(course -> {
            for (int i = 0; i < numberOfReviews; i++) {
                reviews.add(Review.builder()
                        .course(course)
                        .comment("Lorem ipsum dolor sit amet.")
                        .numberOfStars(2 + random.nextInt(4))
                        .build());
            }
        });
        reviewRepository.saveAll(reviews);
    }

}

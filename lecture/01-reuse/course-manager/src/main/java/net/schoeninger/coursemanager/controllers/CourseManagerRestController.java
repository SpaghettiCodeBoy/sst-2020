package net.schoeninger.coursemanager.controllers;

import net.schoeninger.coursemanager.domain.Course;
import net.schoeninger.coursemanager.domain.Review;
import net.schoeninger.coursemanager.services.CourseManagerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.ServletWebRequest;

import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CourseManagerRestController {

    private CourseManagerService courseManagerService;

    public CourseManagerRestController(CourseManagerService courseManagerService) {
        this.courseManagerService = courseManagerService;
    }

    @PostMapping("/courses") @ResponseStatus(HttpStatus.CREATED)
    public Course postCourse(@RequestBody Course course) {
        return courseManagerService.saveCourse(course);
    }

    @GetMapping("/courses")
    public List<Course> getAllCourses() {
        return courseManagerService.findAllCourses();
    }

    @PostMapping("/courses/{courseId}/reviews") @ResponseStatus(HttpStatus.CREATED)
    public Review postReviewToCourse(@PathVariable long courseId, @RequestBody Review review) {
        return courseManagerService.addReviewToCourse(review, courseId);
    }

    @GetMapping("/courses/{courseId}/reviews")
    public List<Review> getAllReviewsForCourse(@PathVariable long courseId) {
        return courseManagerService.findAllReviewsForCourse(courseId);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public void handleValidationExceptions(ConstraintViolationException e, ServletWebRequest request) throws IOException {
        request.getResponse().sendError(HttpStatus.BAD_REQUEST.value(), e.getLocalizedMessage());
    }

}

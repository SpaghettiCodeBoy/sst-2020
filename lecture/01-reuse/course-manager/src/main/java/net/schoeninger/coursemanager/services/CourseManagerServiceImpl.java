package net.schoeninger.coursemanager.services;

import net.schoeninger.coursemanager.domain.Course;
import net.schoeninger.coursemanager.domain.Review;
import net.schoeninger.coursemanager.repositories.CourseRepository;
import net.schoeninger.coursemanager.repositories.ReviewRepository;
import net.schoeninger.coursemanager.utils.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

@Service
@Validated
public class CourseManagerServiceImpl implements CourseManagerService {

    private CourseRepository courseRepository;
    private ReviewRepository reviewRepository;

    public CourseManagerServiceImpl(CourseRepository courseRepository, ReviewRepository reviewRepository) {
        this.courseRepository = courseRepository;
        this.reviewRepository = reviewRepository;
    }

    @Override
    public Course saveCourse(@Valid Course course) {
        return courseRepository.save(course);
    }

    @Override
    public List<Course> findAllCourses() {
        return courseRepository.findAll();
    }

    @Override
    public Review addReviewToCourse(@Valid Review newReview, long courseId) {
        Course course = courseRepository.findById(courseId)
                .orElseThrow(() -> new EntityNotFoundException("Couldn't find Course for ID " + courseId));
        return reviewRepository.save(Review.builder()
                .course(course)
                .numberOfStars(newReview.getNumberOfStars())
                .comment(newReview.getComment())
                .build());
    }

    @Override
    public List<Review> findAllReviewsForCourse(long courseId) {
        Course course = courseRepository.findById(courseId)
                .orElseThrow(() -> new EntityNotFoundException("Couldn't find Course for ID " + courseId));
        return reviewRepository.findAllByCourse(course);
    }

}

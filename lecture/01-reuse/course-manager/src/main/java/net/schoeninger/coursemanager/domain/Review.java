package net.schoeninger.coursemanager.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Entity
@NoArgsConstructor @AllArgsConstructor @Builder @Getter
public class Review extends BaseEntity {

    @Min(0) @Max(5)
    private int numberOfStars;

    @NotBlank
    @Column(nullable = false)
    private String comment;

    @ManyToOne(optional = false)
    @JsonIgnore
    private Course course;

}

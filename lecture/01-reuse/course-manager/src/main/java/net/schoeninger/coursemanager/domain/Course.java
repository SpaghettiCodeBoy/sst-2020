package net.schoeninger.coursemanager.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

@Entity
@NoArgsConstructor @AllArgsConstructor @Builder @Getter
public class Course extends BaseEntity {

    @NotBlank
    @Column(nullable = false)
    private String title;

}

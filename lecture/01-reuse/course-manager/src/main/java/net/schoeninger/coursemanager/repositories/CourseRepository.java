package net.schoeninger.coursemanager.repositories;

import net.schoeninger.coursemanager.domain.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}

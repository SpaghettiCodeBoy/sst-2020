package net.schoeninger.coursemanager.repositories;

import net.schoeninger.coursemanager.domain.Course;
import net.schoeninger.coursemanager.domain.Review;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Long> {

    List<Review> findAllByCourse(Course course);

}
